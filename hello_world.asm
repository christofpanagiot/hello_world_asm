; hello_world.asm
; Assembly Language
; Author: Christoforos Panagiotoudis

section .data
	message: db "Hello World!", 0xA	; String to be printed
	message_length equ $ - message	; Length of the string

section .text
	global _start		; Declaration of Linker
	
_start:				; Entry Point of Linker
	mov eax, 0x4		; System Call Write
	mov ebx, 1		; File Description (stdout)
	mov ecx, message	; Message
	mov edx, message_length	; Message Length
	int 0x80		; Call Kernel
	
	; EXIT
	
	mov eax, 0x1	; System Call Exit
	mov ebx, 0	; Exit 0
	int 0x80	; Call Kernel
